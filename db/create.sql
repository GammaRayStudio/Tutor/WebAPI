-- app_info
drop table if exists app_info;
create table app_info (
	id int auto_increment primary key ,
	name	nvarchar(50),
	version	nvarchar(30),
	author	nvarchar(50),
	date	datetime,
	remark	nvarchar(100)
);

