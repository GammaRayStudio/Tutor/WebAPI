package com.enoxs.controller;

import com.enoxs.application.app.AppInfoApplication;
import com.enoxs.domain.app.repo.entity.AppInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller
 *
 * + API 接口
 * + Application 實作接口功能
 */
@RestController
@RequestMapping("/AppInfo")
public class AppInfoController {
    private final static Logger logger = LoggerFactory.getLogger(AppInfoController.class);

    @Autowired
    private AppInfoApplication appInfoApplication;

    @Value("${app.db.account}")
    private String ac;

    @Value("${app.db.password}")
    private String pw;
    /**
     * Db Setting
     */
    @GetMapping("/DbConfig")
    public String getDbConfig(){
        StringBuffer sb = new StringBuffer(32);
        sb.append("Account = " + ac + "\n");
        sb.append("Password = " + pw + "\n");
        return sb.toString();
    }

    /**
     * This App Info
     */

    @GetMapping("/this")
    public AppInfo getAppInfo(){
        logger.info("getAppInfo()");
        return appInfoApplication.getAppInfo();
    }

    /**
     * 列表
     */
    @GetMapping
    public List<AppInfo> findAll() {
        logger.info("findAll()");
        return appInfoApplication.findAll();
    }

    /**
     * 單筆訊息
     */
    @GetMapping("/{id}")
    public AppInfo findById(@PathVariable("id") Long id) {
        logger.info("findById()");
        return appInfoApplication.findById(id);
    }

    /**
     * 保存數據
     */
    @PostMapping
    public AppInfo create(@RequestBody AppInfo appInfo) {
        logger.info("create()");
        return appInfoApplication.create(appInfo);
    }

    /**
     * 修改數據
     */
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @RequestBody AppInfo appInfo) {
        logger.info("update()");
        appInfo.setId(id);
        appInfoApplication.update(appInfo);
    }

    /**
     * 刪除數據
     */
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        logger.info("delelet()");
        appInfoApplication.delete(id);
    }


}
