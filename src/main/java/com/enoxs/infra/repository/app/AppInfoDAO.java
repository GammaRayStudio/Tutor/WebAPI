package com.enoxs.infra.repository.app;

import com.enoxs.infra.repository.app.po.AppInfoPO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * DAO : Data Access Object
 * 數據訪問對象,此對象用於訪問數據庫。
 *
 * 結合 PO 使用，DAO 中包含了各種數據庫的操作方法。
 * 通過它的方法,結合PO對數據庫進行相關的操作。
 *
 * 除一般數據庫的CRUD操作外，可在此介面中可進行擴充，客製化業務邏輯所需的數據庫操作方法。
 */
@Repository
public interface AppInfoDAO extends CrudRepository<AppInfoPO,Long>{
//    @Query("select * from app_info appInfo where appInfo.name:name")
//    public AppInfoPO findAppInfoByName(@Param("name") String name);

}
