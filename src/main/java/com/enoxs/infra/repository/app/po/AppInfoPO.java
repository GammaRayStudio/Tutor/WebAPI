package com.enoxs.infra.repository.app.po;


import javax.persistence.*;
import java.util.Date;

/**
 * PO : Persistant Object
 *
 * 持久對象 , 可以看成是與數據庫中的表相映射的 java對象。
 *
 * 最簡單的PO就是對應數據庫中某個表中的一條記錄，多個記錄可以用PO的集合。
 * PO中應該不包含任何對數據庫的操作。
 */
@Entity
@Table(name = "app_info")
public class AppInfoPO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "version")
    private String version;

    @Column(name = "author")
    private String author;

    @Column(name = "date")
    private Date date;

    @Column(name = "remark")
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
