package com.enoxs.application.app;


import com.enoxs.domain.app.repo.entity.AppInfo;
import com.enoxs.domain.app.service.AppInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 應用層：
 *
 * Application -> 應用層服務(Application Service)
 *
 * + Controller <---> Application Service : 一對一關係
 * + Application Service <---> Domain Service : 一對多關係
 *
 * 對映 Controller Api 描述的功能，主要負責統籌調用 Domain Service，
 * 實作使用者案例流程。
 *
 * + Service 與 Service 互相的調用都會在此層進行。
 *
 *
 */
@Service
public class AppInfoApplication {

    @Autowired
    private AppInfoService appInfoService;

    public AppInfo getAppInfo(){
        return appInfoService.getAppInfo();
    }

    public List<AppInfo> findAll(){
        return appInfoService.findAll();
    }

    public AppInfo findById(Long id){
        return appInfoService.findById(id);
    }

    public AppInfo create(AppInfo appInfo){
        return appInfoService.create(appInfo);
    }

    public void update(AppInfo appInfo){
        appInfoService.update(appInfo);
    }

    public void delete(Long id){
        appInfoService.delete(id);
    }

}
