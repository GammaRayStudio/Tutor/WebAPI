package com.enoxs.application.app.dto;


import com.enoxs.domain.app.repo.entity.AppInfo;

/**
 * DTO（Data Transfer Object）：
 * 資料傳輸物件，Service 或 Manager 向外傳輸的物件。
 *
 * 以 Domain Layer 的 Entity 與 VO 為基底，建置外部傳輸物件
 *
 */
public class AppInfoDTO extends AppInfo {
    private String groupId;
    private String groupName;
    private String groupDescription;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }
}
