package com.enoxs.domain.app.repo.entity;

import java.util.Date;

/**
 * Entity :
 * 領域層的實體物件
 *
 * 有唯一值，可識別物件，Domain Service 主要操作的兩種對象之一。
 *
 */
public class AppInfo {
    private Long id;
    private String name;
    private String version;
    private String author;
    private Date date;
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "AppInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", version='" + version + '\'' +
                ", author='" + author + '\'' +
                ", date=" + date +
                ", remark='" + remark + '\'' +
                '}';
    }
}
