package com.enoxs.domain.app.repo.vo;

import java.util.Date;


/**
 * VO: Value Object。
 *
 * 領域層的值對象。
 *
 * 此為範例，可以理解為無唯一值的 Entity，
 *
 * 通常用於業務層(Domain)之間的數據傳遞，和 PO一樣也是僅僅包含數據而已。
 *
 */
public class AppInfoVO {
    private String name;
    private String version;
    private String author;
    private Date date;
    private String remark;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
