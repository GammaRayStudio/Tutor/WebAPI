package com.enoxs.domain.app.service;

import com.enoxs.domain.app.repo.entity.AppInfo;

import java.util.List;

/**
 * 領域層：
 *
 * Service -> 領域層服務(Domain Service)
 *
 * 用於提供核心的業務邏輯功能。
 *
 *
 * ### Application -> @Service
 * + 應用層較接近於使用者，變動的頻率會比較高
 * + 實作流程僅為調用 Domain Service ，複雜度較低
 *
 * 因此應用層服務的 @Service 直接加在 Class 上
 *
 * ### Domain -> @Service
 * + 領域層較接遠離使用者，變動的頻率會比較低
 * + 外部調用的 Service 並不需要知道內部 Service 的實作方法
 *
 * 因此領域層服務建置 interface 提供介面，在 impl/ 目錄下實作服務
 *
 *
 */
public interface AppInfoService {
    AppInfo getAppInfo();
    List<AppInfo> findAll();
    AppInfo findById(Long id);
    AppInfo create(AppInfo appInfo);
    void update(AppInfo appInfo);
    void delete(Long id);
}
