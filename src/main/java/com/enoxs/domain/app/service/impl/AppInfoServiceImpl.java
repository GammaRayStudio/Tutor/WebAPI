package com.enoxs.domain.app.service.impl;


import com.enoxs.domain.app.repo.entity.AppInfo;
import com.enoxs.domain.app.service.AppInfoService;
import com.enoxs.infra.repository.app.AppInfoDAO;
import com.enoxs.infra.repository.app.po.AppInfoPO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Service
public class AppInfoServiceImpl implements AppInfoService {

    private final static Logger logger = LoggerFactory.getLogger(AppInfoServiceImpl.class);

    @Autowired
    private AppInfoDAO appInfoDao;

    @Value("${app_name}")
    private String appName;

    @Value("${app_version}")
    private String appVersion;

    @Value("${app_author}")
    private String appAuthor;

    @Value("${app_remark}")
    private String appRemark;

    @Override
    public AppInfo getAppInfo(){
        AppInfo appInfo = new AppInfo();
        appInfo.setId(Long.valueOf(0));
        appInfo.setName(appName);
        appInfo.setVersion(appVersion);
        appInfo.setDate(new Date());
        appInfo.setAuthor(appAuthor);
        appInfo.setRemark(appRemark);
        return appInfo;
    }


    @Override
    public List<AppInfo> findAll() {
        Iterable<AppInfoPO> lstAppInfoPO = appInfoDao.findAll();
        List<AppInfo> lstAppInfo = new LinkedList<>();
        for (Iterator iterator = lstAppInfoPO.iterator(); iterator.hasNext(); ) {
            AppInfoPO po = (AppInfoPO) iterator.next();
            AppInfo entity = new AppInfo();
            entity.setId(po.getId());
            entity.setName(po.getName());
            entity.setVersion(po.getVersion());
            entity.setAuthor(po.getAuthor());
            entity.setDate(po.getDate());
            entity.setRemark(po.getRemark());
            lstAppInfo.add(entity);
        }
        return lstAppInfo;
    }

    @Override
    public AppInfo findById(Long id) {
        AppInfoPO po = appInfoDao.findById(id).orElse(new AppInfoPO());
        AppInfo entity = new AppInfo();
        entity.setId(po.getId());
        entity.setName(po.getName());
        entity.setVersion(po.getVersion());
        entity.setDate(po.getDate());
        entity.setAuthor(po.getAuthor());
        entity.setRemark(po.getRemark());
        return entity;
    }


    @Override
    public AppInfo create(AppInfo appInfo) {
        AppInfoPO po = new AppInfoPO();
        po.setName(appInfo.getName());
        po.setVersion(appInfo.getVersion());
        po.setDate(appInfo.getDate());
        po.setAuthor(appInfo.getAuthor());
        po.setRemark(appInfo.getRemark());

        appInfoDao.save(po);
        appInfo.setId(po.getId());
        return appInfo;
    }

    @Override
    public void update(AppInfo appInfo) {
        AppInfoPO po = new AppInfoPO();
        po.setId(appInfo.getId());
        po.setName(appInfo.getName());
        po.setVersion(appInfo.getVersion());
        po.setDate(appInfo.getDate());
        po.setAuthor(appInfo.getAuthor());
        po.setRemark(appInfo.getRemark());
        appInfoDao.save(po);
    }

    @Override
    public void delete(Long id) {
        AppInfoPO po = appInfoDao.findById(id).orElse(new AppInfoPO());
        appInfoDao.delete(po);
    }
}
