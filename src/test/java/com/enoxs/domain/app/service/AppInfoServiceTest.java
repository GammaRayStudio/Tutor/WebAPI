package com.enoxs.domain.app.service;

import com.enoxs.AppMain;
import com.enoxs.domain.app.repo.entity.AppInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @RunWith
 * @SpringBootTest 在 SpringBoot 的運行環境下調用 Service，若沒加此二註解，只能測試單純 JUnit 案例
 * @Transactional 使用Spring管理交易
 * Spring會在每一個測試後自動Rollback，即是說，測試完最後不會影響到資料庫
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppMain.class)
@Transactional(propagation = Propagation.REQUIRED)
public class AppInfoServiceTest {

    @Autowired
    private AppInfoService appInfoService;

    private Long appId = Long.valueOf(0);
    private String appName = "StandardProjectWithSpringBoot";
    private String appVersion = "1.0.1";
    private String appAuthor = "Enoxs";
    private Date appDate = new Date();
    private String appRemark = "Spring Boot Standard Sample Project - Version 1.0.1";

    /**
     * 準備好的假資料，完成測試後會被刪除
     */
    private Long createTestData() {
        AppInfo appInfo = new AppInfo();
        appInfo.setName(appName);
        appInfo.setVersion(appVersion);
        appInfo.setAuthor(appAuthor);
        appInfo.setDate(appDate);
        appInfo.setRemark(appRemark);
        appInfoService.create(appInfo);
        return appInfo.getId();
    }

    @Test
    public void getThisAppInfo() {
        AppInfo appInfo = appInfoService.getAppInfo();
        assertEquals(appName, appInfo.getName());
        assertEquals(appVersion, appInfo.getVersion());
        assertEquals(appAuthor, appInfo.getAuthor());
        assertEquals(appRemark, appInfo.getRemark());
    }


    @Test
    public void findAll() {
        createTestData(); // test data

        List<AppInfo> lstAppInfo = appInfoService.findAll();
        for (AppInfo appInfo : lstAppInfo) {
            System.out.println(appInfo.toString());
        }
        assertTrue(lstAppInfo.size() > 0);
    }

    @Test
    public void findById() {
        Long id = createTestData();
        AppInfo appInfo = appInfoService.findById(id);
        System.out.println(appInfo.toString());
        assertEquals(appName, appInfo.getName());
        assertEquals(appVersion, appInfo.getVersion());
        assertEquals(appAuthor, appInfo.getAuthor());
        assertEquals(appDate,appInfo.getDate());
        assertEquals(appRemark, appInfo.getRemark());
    }

    @Test
    public void update() {
        Long id = createTestData(); // test data

        AppInfo appInfo = appInfoService.findById(id);
        appInfo.setName("JavaProject");
        appInfo.setVersion("v1.0.2");
        appInfoService.update(appInfo);

        AppInfo show = appInfoService.findById(id);

        System.out.println(show.toString());

        assertEquals("JavaProject", appInfo.getName());
        assertEquals("v1.0.2", appInfo.getVersion());
        assertEquals(appAuthor, appInfo.getAuthor());
        assertEquals(appDate,appInfo.getDate());
        assertEquals(appRemark, appInfo.getRemark());
    }


    @Test
    public void create() {
        AppInfo appInfo = new AppInfo();
        appInfo.setName(appName);
        appInfo.setVersion(appVersion);
        appInfo.setAuthor(appAuthor);
        appInfo.setDate(appDate);
        appInfo.setRemark(appRemark);
        appInfoService.create(appInfo);
        System.out.println(appInfo.toString());

        assertEquals(appName, appInfo.getName());
        assertEquals(appVersion, appInfo.getVersion());
        assertEquals(appAuthor, appInfo.getAuthor());
        assertEquals(appDate,appInfo.getDate());
        assertEquals(appRemark, appInfo.getRemark());
    }

    @Test
    public void delete() {
        Long deleteId = createTestData(); // test data

        appInfoService.delete(deleteId);

        List<AppInfo> lstAppInfo = appInfoService.findAll();
        for (AppInfo show : lstAppInfo) {
            System.out.println(show.toString());
            assertNotEquals(deleteId,show.getId());
        }
    }
}